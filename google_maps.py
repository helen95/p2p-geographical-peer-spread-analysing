import geocoder
import json


html = '''
	
	<!DOCTYPE html>
<html>
  <head>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<title>Marker Clustering</title>
	<style>
	  /* Always set the map height explicitly to define the size of the div
	   * element that contains the map. */
	  #map {
		height: 100%;
	  }
	  /* Optional: Makes the sample page fill the window. */
	  html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	  }
	</style>
  </head>
  <body>
	<div id="map"></div>
	<script>

	  function initMap() {

		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 2,
		  center: {lat: 55.649274, lng: 37.594497}
		});

		var markers = seed.map(function(location, i) {
		  return new google.maps.Marker({
			position: location,		
			title: "I'm seed",
			animation: google.maps.Animation.DROP,
			icon: "1.bmp"
		  });
		});

		// Add a marker clusterer to manage the markers.
		var markerCluster = new MarkerClusterer(map, markers,
			{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
			
		var markers2 = peer.map(function(location, i) {
		  return new google.maps.Marker({
			position: location,
			title: "I'm peer",
			animation: google.maps.Animation.DROP,
			icon: "2.bmp"
		  });
		});
		
		var markerCluster = new MarkerClusterer(map, markers2,
			{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

		var markers3 = leech.map(function(location, i) {
		  return new google.maps.Marker({
			position: location,
			title: "I'm leech",
			animation: google.maps.Animation.DROP,
			icon: "3.bmp"
		  });
		});

		var markerCluster = new MarkerClusterer(map, markers3,
			{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
		}
	  
		var peer = [$peer$]

		var seed = [$seed$]

		var leech = [$leech$]

	</script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
	</script>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1Z-UBAH3xN6VOI28JjjnOYRWBTDEJWXY&callback=initMap">
	</script>


  </body>
</html>


'''

#{"percent": 1000, "torHash": "902D265C11DC5D766639146426273E9B294F8B54", "region": "Error", "abs time": "2017-11-14 08:13:59", "ip": "94.244.94.188", "speed": 1163130, "local_time": "Error", "organization": "Error", "city": "Error", "time_zone": "Error"}


def getLatLng(ip):
	return geocoder.ip(ip).latlng

with open("torrents_google.txt") as file:
	lines = file.readlines()
	seed = []
	peer = []
	leech = []
	try:
		for line in lines:
			if len(line) == 0:
				continue
			data = json.loads(line)
			lat, lng = getLatLng(data['ip'])
			speed = data['speed']
			if data["percent"] == 1000:
				seed.append("{lat: %f, lng: %f},\n" % (lat, lng))
			elif data["percent"] == 0:
				leech.append("{lat: %f, lng: %f},\n" % (lat, lng))
			else:
				peer.append("{lat: %f, lng: %f, ip: %s},\n" % (lat, lng, str(data['ip'])))
	except TypeError:
		print("exception")
	html = html.replace("$peer$", "".join(set(peer)))
	html = html.replace("$seed$", "".join(set(seed)))
	html = html.replace("$leech$", "".join(set(leech)))

	with open("map.html", "w") as res:
		res.write(html)
			




