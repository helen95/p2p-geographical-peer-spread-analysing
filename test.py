import json

with open("torrents.txt", "r") as f:
    L = 1
    for line in f.readlines():
        print("------------------%d-------------------" % L)
        L += 1
        res = {}
        line = json.loads(line)

        if len(line) == 0:
            continue

        for element in line:
            if element["torHash"] in res:
                res[element["torHash"]].add(element["ip"])

            else:
                res[element["torHash"]] = {element["ip"]}

        max_hash = ""
        m = 0
        for key in res:
            if len(res[key]) > m:
                m = len(res[key])
                max_hash = key

        head = res.pop(max_hash)

        res = {
            (max_hash, tuple(head)) : res
        }
        ip_counter = {}

        head_ips = set(list(res.keys())[0][1])

        for ip in head_ips:
            if ip in ip_counter:
                ip_counter[ip] += 1
            else:
                ip_counter[ip] = 1

        for linked_torrents in res.values():
            for linked_ips in linked_torrents.values():
                for ip in linked_ips:
                    if ip in ip_counter:
                        ip_counter[ip] += 1
                    else:
                        ip_counter[ip] = 1



        for key,v in [(k, ip_counter[k]) for k in sorted(ip_counter, key=ip_counter.get)][-5:]:
            print("%s->%d" % (key,v))

        print("Count hashes: ",len( list( res.values() )[0] ))

# import json
#
# print(json.loads('[{"ip":156}]')[0])