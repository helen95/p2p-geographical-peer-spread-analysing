\begin{algorithm}
\caption{Algorithm 1}\label{euclid}
\begin{algorithmic}[1]
\Procedure{DetectingTorrents}{}
%\State{$urlBase \gets "https://rutracker-org.appspot.com/forum/"$} 
\State $\textit{logInTracker()}$
\State $\textit{torrents} \gets \textit{getHashesFromDB()}$
\State{$id \gets \textit{getRandomTorrent(torrents)}$} 
\State{$username \gets getProfileUsername(id)$}
\State{$hashes \gets getAllUserHashes(username)$}
\For{$hash$ $\textbf{in}$ $hashes$}
\State{$magnetURL \gets getMagnetUrl(hash)$}
\State{$html \gets startDownload(magnetURL)$}
\State{$peers \gets getPeers(html)$}
\For{$peer$ $\textbf{in}$ $peers$}
\State{$peerInfo \gets getPeerInfo(peer)$}
\State{$writeDataInFile(peerInfo, peerInfoFileName)$}
\EndFor
\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}