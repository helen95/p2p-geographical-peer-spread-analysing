import math
import json
import geocoder

def getLatLng(ip):
    return geocoder.ip(ip).latlng

def dist(llat1, llong1, llat2, llong2):
    rad = 6372795
    
    #в радианах
    lat1 = llat1*math.pi/180.
    lat2 = llat2*math.pi/180.
    long1 = llong1*math.pi/180.
    long2 = llong2*math.pi/180.

    #косинусы и синусы широт и разницы долгот
    cl1 = math.cos(lat1)
    cl2 = math.cos(lat2)
    sl1 = math.sin(lat1)
    sl2 = math.sin(lat2)
    delta = long2 - long1
    cdelta = math.cos(delta)
    sdelta = math.sin(delta)

    #вычисления длины большого круга
    y = math.sqrt(math.pow(cl2*sdelta,2)+math.pow(cl1*sl2-sl1*cl2*cdelta,2))
    x = sl1*sl2+cl1*cl2*cdelta
    ad = math.atan2(y,x)
    dist = ad*rad

    #вычисление начального азимута
    x = (cl1*sl2) - (sl1*cl2*cdelta)
    y = sdelta*cl2
    z = math.degrees(math.atan(-y/x))

    if (x < 0):
        z = z+180.

    z2 = (z+180.) % 360. - 180.
    z2 = - math.radians(z2)
    anglerad2 = z2 - ((2*math.pi)*math.floor((z2/(2*math.pi))) )
    angledeg = (anglerad2*180.)/math.pi

    #print('Distance >> %.0f' % dist, ' [meters]')
    #print('Initial bearing >> ', angledeg, '[degrees]')
    return dist


center_coords = {'lat': 55.649274, 'lng': 37.594497}
usr_coords = {}
usr_speeds = {}


with open("torrents_google.txt", "r") as f:
    res = {}

    for element in f.readlines():
        element = json.loads(element)
        if len(element) == 0:
            continue
        
        #print(element['ip'])
        llat, llng = 77.1804, 129.55#getLatLng(element['ip'])
        usr_coords[element['ip']] = {'lat': llat, 'lng': llng}
        usr_speeds[element['ip']] = {'speed': element['speed']}
        
        if element["torHash"] in res:
            res[element["torHash"]].add(element["ip"])

        else:
            res[element["torHash"]] = {element["ip"]}

#     print(res)
    max_hash = ""
    m = 0
    for key in res:
        if len(res[key]) > m:
            m = len(res[key])
            max_hash = key

    head = res.pop(max_hash)

    res = {
        (max_hash, tuple(head)) : res
    }
    ip_counter = {}

#         print(res)
    head_ips = set(list(res.keys())[0][1])

#         print(head_ips)
    for ip in head_ips:
        if ip in ip_counter:
            ip_counter[ip] += 1
        else:
            ip_counter[ip] = 1

    for linked_torrents in res.values():
        for linked_ips in linked_torrents.values():
            for ip in linked_ips:
                if ip in ip_counter:
                    ip_counter[ip] += 1
                else:
                    ip_counter[ip] = 1


#         print(ip_counter)
    for key,v in [(k, ip_counter[k]) for k in sorted(ip_counter, key=ip_counter.get)][-5:]:
        print("%s->%d" % (key,v))

    print("Count hashes: ",len( list( res.values() )[0] )) ## FIX IT

	
dist_and_speed_to_ip = {}

for ip, coord in usr_coords.items(): 
    dist_and_speed_to_ip[ip] = {'dist' : dist(center_coords['lat'], center_coords['lng'], coord['lat'], coord['lng']), 'speed' : usr_speeds[ip]}
	
