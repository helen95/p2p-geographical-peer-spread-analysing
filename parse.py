# -*- coding: utf-8 -*-
import sqlite3
import random
from math import ceil
from urllib.parse import urlparse, parse_qs, urlencode
import json
import time
import urllib.parse
import urllib
import os

from datetime import datetime

import requests
from bs4 import BeautifulSoup

s_torrent = requests.Session()	# Создали переменную сессия через которую посылаем запросы get/post данная сессия СОХРАНЯЕТ cookies
s_torrent.auth = ('admin', '123') # настраиваем сессию чтобы все запросы были вот с таким логином и паролем. В веб интерфейсе торнента задаем точно такие же, иначе не авторизируемся.
s_torrent.headers.update(
	{
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
		"Connection": "keep-alive"}) # чтобы не разрывалась связь и торент пусть думает что мы к нему обращаемся через Mozilla


def getToken():
	res = s_torrent.get("http://127.0.0.1:7070/gui/token.html", timeout=10) # получаем токен с этого адресса (важно указать правильный порт 8080 в веб интерфейсе торента иначе работать не будет!!!!)
	time.sleep(10)
	res = s_torrent.get("http://127.0.0.1:7070/gui/token.html", timeout=10)
	soup = BeautifulSoup(res.text, "html.parser")
	return soup.text


TOKEN = getToken() # беру токен
print(TOKEN)

def sendRequest(param):
	params = urllib.parse.urlencode(param) # берет словарь и переводит в URL запрос

	res = s_torrent.get("http://127.0.0.1:7070/gui/?" + params, timeout=10) #

	return res.text # возвращаем соответствующий ответ в зависимости от params значения

# определим входные param
def getPeersParams(hash): # возвращает словарь, который говорит дать данные по пиру
	return {

		'action': 'getpeers',
		'hash': hash,
		'token': TOKEN
	}


def getAddUrlParams(s): # добавь торент на закачку
	return {
		'action': 'add-url',
		's': 'magnet:?xt=urn:btih:' + s,
		'token': TOKEN
	}


def getRemoveParams(hash): # удали только торент из загрузки
	return {
		'action': 'remove',
		'hash': hash,
		'token': TOKEN
	}


def getRemoveDataParams(hash): # удали загшруженные данные
	return {
		'action': 'removedata',
		'hash': hash,
		'token': TOKEN
	}

def getCoords(ip): # получаем всю возможную информацию по ip  с указанного сайта с учетом неотклика сайта. На получение информации дается максимум 2 попытки. Время ожидания 65 сек.Интервал между обработками 5 сек.
	tries = 2
	ip_fields = {
		"location": 0,
		'region': 1,
		'city': 2,
		'provider': 7,
		'organization': 8,
		'time_zone': 14,
		'local_time': 15
	}
	ERROR = {
		'location': "Error",
		'region': "Error",
		'city': "Error",
		'provider': "Error",
		'organization': "Error",
		'time_zone': "Error",
		'local_time': "Error"
	}

	while tries > 0:
		time.sleep(5)
		try:
			res = s_torrent.post("https://whoer.net/checkwhois", data={"partial": "1", "host": ip}, timeout=10)

			soup = BeautifulSoup(res.text, "html.parser")

			if (res.status_code == 200) and (soup.find("b", text="Location") is not None):
				Massive_IP = [i.text.strip() for i in soup.find_all('dd')]

				return {
					'location': Massive_IP[ip_fields['location']],
					'region': Massive_IP[ip_fields['region']],
					'city': Massive_IP[ip_fields['city']],
					'provider': Massive_IP[ip_fields['provider']],
					'organization': Massive_IP[ip_fields['organization']],
					'time_zone': Massive_IP[ip_fields['time_zone']],
					'local_time': Massive_IP[ip_fields['local_time']]
				}

			else:
				if res.status_code == 200:
					print("sleep")
					time.sleep(65)

				tries -= 1
		except requests.exceptions.ConnectionError:
			print("Connection error")
			return ERROR
		except requests.exceptions.ReadTimeout:
			print("ReadTimeout error")
			return ERROR

def run_torrent_google_map(): # по заранее подготовленному нами файлу с хэшами из базы hashes.txt (см.код ниже - вспомагательный файл при каждом запуске программы перезаписывается нужными нам данными) считываем каждый хэш и обрабатываем в соответствии с вышеуказанными функциями. Роль файла hashes.txt что в него записываются случайным образом выбранные из базы хэши (не более 500). Итоговая информация записывается в файл torrents_google.txt

	with open('hashes.txt', 'r') as f:
		HASHES_COUNT = len(f.readlines())
		f.seek(0)
		HASH_LEN = 40 + 2
		SLEEP = 250

		hashes = []
		print("get %d random hashes" % HASHES_COUNT)
		for i in range(HASHES_COUNT):
			f.seek(i * (HASH_LEN - 1))
			if i == HASHES_COUNT - 1:
				hash = f.readline()[:]
			else:
				hash = f.readline()[:-1]

			time.sleep(0.7) # Каждый хэш добавляется на закачку через 0.7 сек. иначе торент "не успевает" и пропускуает.
			print(hash)
			sendRequest(getAddUrlParams(hash)) # созданная нами функция добавляет торент на закачку (см.выше)
			hashes.append(hash)

		time.sleep(SLEEP) # даем время подсоединиться к пирам
		peers = None
		for torHash in hashes:
			html = sendRequest(getPeersParams(torHash)) # созданная нами функция получаем информацию по пирам
			j = json.loads(html) # записываем в джэйсон
			try:
				peers = j['peers'][1]

				for i in peers:
					print(i[1], i[7], i[8]) # нас интересует только ip, процент и скорость

					coords = getCoords(i[1]) # созданная нами функция для получения всей необходимой информации по каждому ip.
					if coords is None:
						continue
					print('hey', coords)
					absTime = datetime.now()
					resultData = {"torHash": torHash,
								  "ip": i[1],
								  "percent": i[7],
								  "speed": i[8],
								  "region": coords["region"],
								  #"lat": coords[1],
								  #"lng": coords[2],
								  "city": coords["city"],
								  "organization": coords['organization'],
								  'time_zone': coords['time_zone'],
								  'local_time': coords['local_time'],
								  "abs time": absTime.strftime("%Y-%m-%d %H:%M:%S")
								  }

					with open("torrents_google.txt", 'a') as f2: # записываем в итоговый файл. По которому будем потом строить карту.
						if i[1]:
							f2.write(json.dumps(resultData))
							f2.write("\n")
			except KeyError:
				pass
			except AttributeError:
				print("Error", torHash, peers[0][1])

			sendRequest(getRemoveDataParams(torHash)) # удаляем закаченные данные в корзину
			sendRequest(getRemoveParams(torHash)) # удаляем торент из закачки

def run_torrent_detecting(): # по заранее подготовленному нами файлу с хэшами из базы (см.код ниже - вспомагательный файл не учавствующий нигде, при каждом запуске программы перезаписывается нужными нам данными) считываем каждый хэш и обрабатываем в соответствии с вышеуказанными функциями.
	# разница только в сути файла hashes.txt Здесь он заполняется ОДНИМ случайным хэшем из базы и всеми остальными хэшами, которые принадлежат автору этого случайного хэша (т.е. образно все созданные раздачи случайно выбранного автора, но не более 500) и записывается итоговая информация в другой файл torrents.txt

	with open('hashes.txt', 'r') as f:
		HASHES_COUNT = len(f.readlines())
		f.seek(0)
		HASH_LEN = 40 + 2
		SLEEP = 250

		hashes = []
		print("get %d random hashes" % HASHES_COUNT)
		for i in range(HASHES_COUNT):
						
			f.seek(i * (HASH_LEN - 1))
			if i == HASHES_COUNT - 1:
				hash = f.readline()[:]
			else:
				hash = f.readline()[:-1]
			time.sleep(0.7)
			sendRequest(getAddUrlParams(hash))
			hashes.append(hash)

		time.sleep(SLEEP)
		peers = None
		result = []
		for torHash in hashes:
			html = sendRequest(getPeersParams(torHash))
			print(html)
			j = json.loads(html)
			try:
				peers = j['peers'][1]

				for i in peers:
					print(i[1], i[7], i[8])
					coords = getCoords(i[1])
					if coords is None:
						continue
					print(coords)
					absTime = datetime.now()
					resultData = {"torHash": torHash,
								  "ip": i[1],
								  "percent": i[7],
								  "speed": i[8],
								  "region": coords["region"],
								  # "lat": coords[1],
								  # "lng": coords[2],
								  "city": coords["city"],
								  "organization": coords['organization'],
								  'time_zone': coords['time_zone'],
								  'local_time': coords['local_time'],
								  "abs time": absTime.strftime("%Y-%m-%d %H:%M:%S")
								  }
					if i[1]:
						result.append(resultData)

			except KeyError:
				pass
			except AttributeError:
				print("Error", torHash, peers[0][1])

			sendRequest(getRemoveDataParams(torHash))
			sendRequest(getRemoveParams(torHash))

		with open("torrents.txt", 'a') as f2:
			f2.write(json.dumps(result))
			f2.write("\n")

with requests.Session() as session: # сессия аналогичная s_torrent через которую можно задавать параметры и она сохраняет cookies

	URL_BASE = "https://rutracker-org.appspot.com/forum/"#"https://rutracker.org/forum/"

	def get_torrent_from_sqlite():
		conn = sqlite3.connect('torrents.db3')
		c = conn.cursor()
		c.execute('''select file_id, hash_info from torrent''')
		return c.fetchall() # распарси таблицу торент и верни массив укеазанных пар

	#def login_in_rutracker():
	#	session.headers.update({"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36", "Connection":"keep-alive", "Host":"rutracker-org.appspot.com", "Update-Insecure-Requests":"1", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8", "Accept-Language":"ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4", "Pragma":"no-cache", "Cache-Control":"no-cache"})
	#	# чтобы не разрывалась связь и торент пусть думает что мы к нему обращаемся через Mozilla
	#	try:
	#		session.cookies.set("bb_session","0-42253896-bC8HCa0NBiftZswuo6ke", path="/forum/", domain=".rutracker-org.appspot.com")
	#		session.post(URL_BASE + "login.php", data={"login_username": "XrunAlban", "login_password": "12345678"})
	#		write_in_file(get_html("https://rutracker-org.appspot.com/forum/index.php"))
	#	except requests.RequestException:
	#		print("RequestException error")
			
	def login_in_rutracker():
		login = 'https://rutracker-org.appspot.com/forum/login.php'
		l = session.get(login) 
		soup = BeautifulSoup(l.text,'lxml')
		values = {'loginname': 'XrunAlban', 'password': r'12345678','security_token' : soup.find('input')['value'], 'login_ticket' : soup.find('input').next_sibling.next_sibling['value'], 'resolution' : '', 'device_pixel_ratio' : '1'}
		
		r = session.post(login, data = values)
		#print(soup.find('input', {'name':'cap_sid'})['value'])
		captcha = "123"#input()
		CREDS = {
			'login':  u'Вход',
			'login_username': os.environ.get('TRACKER_USER', 'XrunAlban'),
			'login_password': os.environ.get('TRACKER_PASSWORD', '12345678'),
			'cap_sid':'gcxRCXPIVqp2QaBADxA5', 
			'cap_code_b002522144f4f9a96dda6e15433785e4':str(captcha),
			'security_token' : soup.find('input')['value'], 
			'login_ticket' : soup.find('input').next_sibling.next_sibling['value'], 
			'resolution' : '', 
			'device_pixel_ratio' : '1',
			'redirect': 'index.php'
		}
		r = session.post(login, data = CREDS)

	def get_html(address): # получаем html	страницу

		return session.get(address).text

	def get_author_torrent_list(html):
		soup = BeautifulSoup(html, "html.parser")
		th = soup.find("th", text="Раздачи:")
		if th is not None:
			link = th.next_sibling.next_sibling.find("a")

			if link is not None:
				return URL_BASE + link.get('href')

	def get_torrents_urls_list(html):

		soup = BeautifulSoup(html, "html.parser")
		#print(soup.find("span", text="(max: 500)"))
		count_torrents = int(soup.find("span", text="(max: 500)").previous_sibling.split("Результатов поиска: ")[1])

		print("count torrents %d" % count_torrents)

		result = []

		table = soup.find(id="tor-tbl")
		if table is not None:
			table = table.tbody.find_all("tr")

			for tr in table:
				result.append(URL_BASE + tr.find_all('a')[1]['href']) #TODO: Собираются ВСЕ ссылки из таблицы (и на форум, и на тему, и на автора...)
		else:
			print("\ttable is None")

		next_url = soup.find("a", text="След.")
		if next_url is not None:
			url = URL_BASE + next_url['href']
			if url is not None:
				for i in range(ceil(count_torrents / 50)-1):

					tmp_soup = BeautifulSoup(get_html(url), "html.parser")

					table = tmp_soup.find(id="tor-tbl")
					if table is not None:
						table = table.tbody.find_all("tr")

						for tr in table:
							result.append(URL_BASE + tr.find_all('a')[1]['href'])


					params = parse_qs(urlparse(url).query)
					params['start'][0] = str(int(params['start'][0]) + 50)

					query = []

					for k, v in params.items():
						query.append("%s=%s" % (k, v[0]))

					url = "https://rutracker-org.appspot.com/forum/tracker.php?%s" % "&".join(query)
			else:
				print("\turl is None")
		else:
			print("\tnext url is None")
		print("\t return ", result)
		return result

	def get_profile_url(html):
		soup = BeautifulSoup(html, "html.parser")
		if soup.find("p", "nick", text="Гость") is None:
			profile = soup.find('a', text="[Профиль]")
			#print(profile, "_______________")
			if profile is not None:
				return URL_BASE + profile['href']


	def get_hash(html):
		soup = BeautifulSoup(html, "html.parser")
		if soup.find(id="tor-hash") is not None:
			return soup.find(id="tor-hash").text
		else:
			magnet = "magnet:?xt=urn:btih:"
			a = soup.find("a", "magnet-link")
			if a is not None:
				return a.get('href')[len(magnet):len(magnet)+40]


	def get_profile_username(html):
		soup = BeautifulSoup(html, "html.parser")
		return soup.head.title.text

	def start_for_detecting():

		print("login rutracker")
		login_in_rutracker()

		print("get torrent from db")
		torrents = get_torrent_from_sqlite()

		while True:
			id, hash = torrents[random.randint(0, len(torrents))]
			print("choosen torrent id-%d hash-%s" % (id, hash))
			topic_addr = URL_BASE + "viewtopic.php?t=%d" % (id)
			
			print("get topic html")
			topic_html = get_html(topic_addr)
			
			print("get_profile_url")
			profile_url = get_profile_url(topic_html)

			if profile_url is None:
				continue

			print("get_profile_html")
			profile_html = get_html(profile_url)

			print("get username")
			username = get_profile_username(profile_html)

			print("get author torrents url")
			author_torrents_url = get_author_torrent_list(profile_html)

			print("get author torrents list html")
			torrents_list_html = get_html(author_torrents_url)
			
			print("get author torrents list urls")
			#print(torrents_list_html)
			with open("html.txt", "w") as f1:
				f1.write(torrents_list_html)
			urls = get_torrents_urls_list(torrents_list_html)

			print("get hashes by urls")
			hashes = [j for j in [ get_hash(get_html(i)) for i in urls	] if j is not None]

			print("write hashes to file")
			with open("hashes.txt", "w") as f:
				f.write("\n".join(hashes))

			print("start parser")
			run_torrent_detecting()
			break

	def get_elements_from_array(arr, count):
		res = []
		while len(res) < count:
			id, hash = arr[random.randint(0, len(arr))]
			if hash not in res:
				res.append(hash)
		return res

	def write_in_file(text):
		with open("1.txt", "w") as f:
			f.write(text)
		
	def start_for_google_map(count):

		print("login rutracker")
		login_in_rutracker()

		print("get torrent from db")
		torrents = get_torrent_from_sqlite()

		while True:

			print("write hashes to file")
			with open("hashes.txt", "w") as f:
				f.write("\n".join(get_elements_from_array(torrents, count)))

			print("start parser")
			run_torrent_google_map()
			break

	variant=int(input('выбери программу 1-start_for_google_map, 2-start_for_detecting\n'))
	if variant==1:
		count=int(input('количество случайных hases из базы\n'))
		start_for_google_map(count)
	if variant==2:
		start_for_detecting()
s_torrent.close()
